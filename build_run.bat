@echo off
set GOARCH=amd64
echo Building...
go build -o .\build\evil.exe -mod=vendor && cls && .\build\evil.exe -p ./phishlets -t ./redirectors -developer -debug
